/* global ko: false, Masonry: false */
import Card from './Card.js';
import { getIndices, shuffle } from './functions.js';

/**
 * AE View
 * @namespace AeView
 */

/**
 * the VM for the game
 * @class AeView
 * @memberOf AeView
 */
class AeView{
	/**
	 * constructor
	 */
	constructor(){
		this.addToDeck = this.addToDeck.bind(this);
		this.allCards = ko.observableArray([]);
		this.cancelOngoing = this.cancelOngoing.bind(this);
		this.cardIndex = 0;
		this.cardsArray = ko.observableArray([]);
		this.cardURL = ko.observable();
		this.deckMade = ko.observable(false);
		this.enemyDeck = ko.observableArray([]);
		this.filterKeyword = ko.observable();
		this.isOngoing = ko.observableArray([]);
		this.masonry = [];
		this.results = ko.observableArray([]);
		this.switchTabs = this.switchTabs.bind(this);
		this.throttleKeywords = ko.computed(this.filterKeyword).extend({ throttle: 500 });
		this.topCard = ko.observable();

		this.getCards();

		return this;
	};

	/**
	 * Adds a card to the deck
	 * @param {Card} card Card to add to the deck
	 */
	addToDeck(card){
		if(!this.deckMade()){
			if(getIndices(this.enemyDeck(), card).length >= 2){
				this.enemyDeck.remove(card);
			}else{
				this.enemyDeck.push(card);
			}
		}
	};

	/**
	 * removes a card from the ongoing schemes
	 * @param  {Card} card Thec ard to remove from the ongoing array
	 */
	cancelOngoing(card){
		var qty = getIndices(this.isOngoing(), card).length;

		this.isOngoing.remove(card);

		for(qty; qty > 0; --qty){
			this.enemyDeck.push(card);
		}
	};

	/**
	 * [clearDeck description]
	 * @return {[type]} [description]
	 */
	clearDeck(){
		var inputs = document.querySelectorAll('input'),
			columns = document.querySelectorAll('.columns');

		inputs.forEach(element => { element.disabled = false; });
		columns.forEach(element => element.classList.remove('disabled'));
		this.enemyDeck.removeAll();
		this.isOngoing.removeAll();
		this.topCard(null);
		this.cardIndex = 0;
		this.deckMade(false);
	};

	/**
	 * sets up the model when a deck has been created
	 */
	createDeck(){
		var columns = document.querySelectorAll('.columns'),
			inputs = document.querySelectorAll('input');

		inputs.forEach(element => { element.disabled = true; });
		columns.forEach(element => element.classList.add('disabled'));
		this.results([]);
		this.shuffleDeck();
		this.deckMade(true);
	};

	/**
	 * loads all cards from json into the model
	 */
	getCards(){
		var layouts = document.getElementsByClassName('columns');

		fetch('js/cards.json?_=' + (new Date()).getTime(), {
			method: 'get'
		})
			.then(response => response.json())
			.then(data => {
				var ni,
					arr = [];

				for(ni = 0; ni < data.length; ++ni){
					arr = arr.concat(data[ni].cards);
				}

				arr.forEach((item, index, array) => { array[index] = new Card(item); });
				this.cardsArray(arr);
				this.allCards(data);
				for(ni = 0; ni < layouts.length; ++ni){
					this.masonry[ni] = new Masonry(layouts[ni], {
						transitionDuration: 0
					});
				}
			});
	};

	/**
	 * gets the results of the keyword filter
	 * @param  {string} keyword keyword to filter cards by
	 */
	getFilterResults(keyword){
		this.results(this.cardsArray().filter(item => item.description.toLowerCase().indexOf(keyword.toLowerCase()) > 0));
	}

	/**
	 * shows the next card in the deck
	 */
	nextCard(){
		if(this.enemyDeck().length <= 0){
			return;
		}

		if(this.cardIndex >= this.enemyDeck().length){
			this.cardIndex = 0;
		}

		this.topCard(this.enemyDeck()[this.cardIndex]);

		if(this.enemyDeck()[this.cardIndex].ongoing === 'true'){
			this.isOngoing.push(this.enemyDeck()[this.cardIndex]);
			this.enemyDeck.splice(this.cardIndex, 1);

			if(this.cardIndex >= this.enemyDeck().length){
				this.cardIndex = 0;
			}

			--this.cardIndex;
		}

		++this.cardIndex;
		if(this.topCard){
			this.cardURL('img/' + this.topCard().id + '.jpg');
		}
	};

	/**
	 * adds random cards to the deck
	 */
	randomDeck(){
		var randScheme,
			total = this.cardsArray().length;

		while(this.enemyDeck().length < 20){
			randScheme = Math.floor(Math.random() * total);

			if(getIndices(this.enemyDeck(), this.cardsArray()[randScheme]).length < 2){
				this.enemyDeck.push(this.cardsArray()[randScheme]);
			}
		}
	};

	/**
	 * randomizes the deck
	 */
	shuffleDeck(){
		var ni;

		for(ni = 0; ni < this.isOngoing().length; ++ni){
			this.enemyDeck.push(this.isOngoing()[ni]);
		}

		this.isOngoing.removeAll();

		this.enemyDeck(shuffle(this.enemyDeck()));
		this.cardIndex = 0;
		this.nextCard();
	};

	/**
	 * Switches between card set tabs
	 * @param  {Object} scope the scope of the element being clicked
	 * @param  {Object} evt   dom click event
	 */
	switchTabs(scope, evt){
		var tabs = Array.apply(null, document.querySelectorAll('.tab')),
			tabBodys = Array.apply(null, document.querySelectorAll('.tab-body')),
			index = tabs.indexOf(evt.target),
			ni;

		tabs.forEach(element => element.classList.remove('active'));
		tabBodys.forEach(element => element.classList.remove('active'));
		evt.target.classList.add('active');
		tabBodys[index].classList.add('active');

		for(ni = 0; ni < this.masonry.length; ++ni){
			this.masonry[ni].layout();
		}
	};
};

export default AeView;
export { AeView };
