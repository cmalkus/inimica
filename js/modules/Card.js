/**
 * Card Class
 * @namespace Card
 */

/**
 * Card data type
 * @class Card
 * @memberOf Card
 */
class Card{
	/**
	 * constructor
	 * @param  {Object}		obj 			object to make the card from
	 * @param  {string}		obj.description	the card text
	 * @param  {number}		obj.id			the card id
	 * @param  {Boolean}	obj.ongoing		whether the scheme is ongoing
	 * @param  {string}		obj.title		the card name
	 */
	constructor(obj){
		try{
			if(!obj) throw new Error('No oject passed to constructor');

			try{
				if(!obj.hasOwnProperty('description')) throw new Error('No card description found');
				if(!obj.hasOwnProperty('id')) throw new Error('No card ID found');
				if(!obj.hasOwnProperty('ongoing')) throw new Error('No card ongoing found');
				if(!obj.hasOwnProperty('title')) throw new Error('No card title found');
			} catch (error){
				console.error(error);
				return;
			}
		} catch (error){
			console.error(error);
			return;
		}

		this.description = obj.description;
		this.id = parseInt(obj.id);
		this.ongoing = obj.ongoing === 'true';
		this.title = obj.title;

		return this;
	}
};

export default Card;
export { Card };
