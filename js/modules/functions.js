/**
 * Gets the indicies of a value in an array
 * @param  {Array} arr array to search
 * @param  {string} val value to search for
 * @return {number[]}     array of indices where the value appeard
 */
var getIndices = (arr, val) => {
	var ni,
		indexes = [];

	for(ni = 0; ni < arr.length; ni++){
		if(parseInt(arr[ni].id) === parseInt(val.id)){
			indexes.push(ni);
		}
	}

	return indexes;
};

/**
 * Shuffles array items
 * @param  {Array} o array to shuffle
 * @return {Array}   shuffled array
 */
var shuffle = o => {
	var j, x, i;

	for(j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

export { getIndices, shuffle };
