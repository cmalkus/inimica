/* global ko: false */
import { AeView } from './modules/AeView.js';
import { getIndices, shuffle } from './modules/functions.js';

window.getIndices = getIndices;
window.shuffle = shuffle;
window.uiStuff = document.querySelectorAll('.uiStuff');
window.view = new AeView();

ko.applyBindings(window.view);
window.view.throttleKeywords.subscribe(value => {
	var ni;

	window.view.getFilterResults(value);

	for(ni = 0; ni < window.view.masonry.length; ++ni){
		window.view.masonry[ni].reloadItems();
		window.view.masonry[ni].layout();
	}
});

if(window.outerWidth > 600){
	window.onscroll = () => {
		if(window.scrollY > 60){
			window.uiStuff[0].style.top = (window.scrollY - 60) + 'px';
		}else{
			window.uiStuff[0].style.top = '0px';
		}
	};
}
